#ifndef __RTE_PIE_H_INCLUDED__
#define __RTE_PIE_H_INCLUDED__
#include <bits/stdint-uintn.h>

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <rte_common.h>
#include <rte_random.h>
#include <rte_timer.h>

#define DROP                0
#define ENQUEUE             1
#define PIE_FIX_POINT_BITS  13
#define PIE_PROB_BITS       31
#define PIE_MAX_PROB		((1LL<<PIE_PROB_BITS)-1)

struct rte_pie_params{
	uint32_t target_delay;
	uint32_t t_update;
	uint32_t mean_pkt_size;
	uint32_t max_burst;
	float alpha;
	float beta;
};

struct rte_pie_config {
	uint32_t target_delay;
	uint32_t t_update;
	uint32_t alpha;
	uint32_t beta;
	uint32_t mean_pkt_size;
	uint32_t max_burst;
};

struct rte_pie_rt {
	int64_t drop_prob;
	uint32_t burst_allowance;
	uint64_t old_qdelay;
	uint64_t cur_qdelay;
	uint64_t accu_prob;
};

struct rte_pie {
	struct rte_pie_config *pie_config;
	struct rte_pie_rt *pie_rt;
};

int rte_pie_config_init(struct rte_pie_config*, uint32_t, uint32_t, uint32_t, uint32_t, double, double);

int rte_pie_data_init(struct rte_pie_rt *);

int rte_pie_drop(struct rte_pie_config *, struct rte_pie_rt *, uint32_t);

int rte_pie_enqueue(struct rte_pie_config *, struct rte_pie_rt *, uint32_t);

static inline uint64_t max(uint64_t a, uint64_t b) {
	return a > b ? a : b;
}

__rte_unused static void rte_pie_calc_drop_prob(
		__attribute__((unused)) struct rte_timer *tim, void *arg) {
	struct rte_pie_rt *pie = (struct rte_pie_rt *) arg;
	struct rte_pie_config *config = pie->pie_config;
	struct rte_pie_rt *pie_rt = pie->pie_rt;
	int p_isneg;
	
	uint64_t cur_qdelay = pie_rt->cur_qdelay;
	uint64_t old_qdelay = pie_rt->old_qdelay;
	uint64_t target_delay = config->target_delay;
	int64_t oldprob;
	int64_t p = config->alpha * (cur_qdelay - target_delay) + \
			config->beta * (cur_qdelay - old_qdelay);

	p_isneg = p < 0;

	if (p_isneg)
		p = -p;

	p *= (PIE_MAX_PROB << 12) / rte_get_tsc_hz();
		
	if (pie_rt->drop_prob < (PIE_MAX_PROB / 1000000)) {
		p >>= 11 + PIE_FIX_POINT_BITS + 12;
	} else if (pie_rt->drop_prob < (PIE_MAX_PROB / 100000)) {
		p >>= 9 + PIE_FIX_POINT_BITS + 12;
	} else if (pie_rt->drop_prob < (PIE_MAX_PROB / 10000)) {
		p >>= 7 + PIE_FIX_POINT_BITS + 12;
	} else if (pie_rt->drop_prob < (PIE_MAX_PROB / 1000)) {
		p >>= 5 + PIE_FIX_POINT_BITS + 12;
	} else if (pie_rt->drop_prob < (PIE_MAX_PROB / 100)) {
		p >>= 3 + PIE_FIX_POINT_BITS + 12;
	} else if (pie_rt->drop_prob < (PIE_MAX_PROB / 10)) {
		p >>= 1 + PIE_FIX_POINT_BITS + 12;
	} else {
		p >>= PIE_FIX_POINT_BITS + 12;
	}

	oldprob = pie_rt->drop_prob;
	
	if (p_isneg) {
		pie_rt->drop_prob = pie_rt->drop_prob - p;
		if (pie_rt->drop_prob > oldprob) {
			pie_rt->drop_prob = 0;
		}
	} else {
		if (pie_rt->drop_prob >= PIE_MAX_PROB / 10 && \
				p > PIE_MAX_PROB / 50 ) {
			p = PIE_MAX_PROB / 50;
		}

		pie_rt->drop_prob += p;

		if (pie_rt->drop_prob < oldprob) {
			pie_rt->drop_prob = PIE_MAX_PROB;
		}
	}
	pie_rt->drop_prob += p;

	if (pie_rt->drop_prob < 0) {
		pie_rt->drop_prob = 0;
	} else {
		if (cur_qdelay/1000 == 0 && old_qdelay/1000 == 0) {
			pie_rt->drop_prob -= pie_rt->drop_prob >> 6;
		}

		if (pie_rt->drop_prob > PIE_MAX_PROB) {
			pie_rt->drop_prob = PIE_MAX_PROB;
		}
	}

	if (pie_rt->burst_allowance < config->t_update) {
		pie_rt->burst_allowance = 0;
	} else {
		pie_rt->burst_allowance -= config->t_update;
	}

	pie_rt->old_qdelay = cur_qdelay;
}

static inline void rte_pie_dequeue(struct rte_pie_rt *pie_rt,
		uint64_t timestamp) {
	uint64_t now = rte_get_tsc_cycles();
	pie_rt->cur_qdelay = now - timestamp;
}

#ifdef __cplusplus
}
#endif

#endif /* __RTE_PIE_H_INCLUDED__ */
